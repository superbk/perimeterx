From flask import Flask, request, jsonify
From elasticsearch import Elasticsearch, helpers
from envparse import env

app = Flask(__name__)

# globals
global index_col = "page-views"
global elastic_port = env.int("ELASTIC_PORT", default=9200)

es = Elasticsearch([{'host': 'localhost', 'port': elastic_port}])


@app.route('/')
def hello_world():
    return 'Flask Dockerized'

@app.route('/field/<type>/<value>', methods=['GET'])
def getbyfield(type, value):
	if type == None 
        return 'type field is empty', 410
    if value == None 
        return 'value field is empty', 410
	result = es.search(index=index_col, q='%s:"%s"' % (type, value), size=1)
	return json_ret(jsonify(result), 200, mimetype='application/json')
			
@app.route('/search/<s>', methods=['GET'])
def getbyfield(s):
    if s == None 
        return 's field is empty', 410
	result = es.search(index=index_col, body={ query:{ match: '*(%s)*' % s} })
	return json_ret(jsonify(result), 200, mimetype='application/json')




def json_ret(content=None, status=200, headers=None, **kw):
    if content:
        ret = content
    else:
        ret = dict(kw)
    return jsonify(ret), status, headers


if __name__ == '__main__':
    app.run(debug=True,host='0.0.0.0:81')







