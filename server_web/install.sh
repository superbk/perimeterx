#Install flask and python env
#————————————————————————————
pip install Flask
git clone http://github.com/pallets/flask.git flask
virtualenv flask/venv
cp app.py flask/
cp requirements.txt flask/
. flask/venv/bin/activate
docker build .
docker run -it -p 81:9200 ubuntu:latest
